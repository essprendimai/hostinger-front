FROM node:8

# Install dependencies
WORKDIR /usr/src/app

# Build the app
RUN npm build

# Expose the app port
EXPOSE 3000

# Start the app
CMD npm start