// @flow

import React, { Component } from 'react';
import './Assets/scss/main.scss';
import {BootstrapTable, TableHeaderColumn, InsertModalFooter, DeleteButton} from 'react-bootstrap-table';
import '../node_modules/react-bootstrap-table/css/react-bootstrap-table.css'
import CreateNewUserBody from './Modals/Body/CreateNewUserBody';
import Loader from 'react-loader-spinner'
import OAuthService from './Api/Services/OAuthService';
import ApiUsersService from './Api/Services/ApiUsersService';

type GetUserResponseDataType = {|
    status: OK | FAIL,
    data: Object<{
        change_password: Number,
        created_as_draft: Boolean,
        created_at: String,
        email: String,
        email_verified_at: String,
        id: Number,
        is_admin: Boolean,
        is_draft: Boolean,
        name: String,
        updated_at: String,
    }>,
|};

type GetAllUsersResponseDataType = Array<GetUserResponseDataType>;

type State = {
    users: GetAllUsersResponseDataType,
    loading: Boolean,
};
type Props = {};

class App extends Component<Props, State> {
    state: State = {
        users: null,
        loading: true
    };

    oauth = null;
    apiUsers = null;

    constructor(props)
    {
        super(props);

        this.oauth = new OAuthService(process.env.REACT_APP_BACKEND_URL, process.env.REACT_APP_API_CLIENT_ID, process.env.REACT_APP_API_CLIENT_SECRET, process.env.REACT_APP_API_CLIENT_USERNAME, process.env.REACT_APP_API_CLIENT_PASSWORD);
        this.apiUsers = new ApiUsersService(this.oauth);

        this.setUsers();
    }

    async setUsers(): void
    {
        this.setState({
            users: await this.apiUsers.getAllUsers(),
            loading: false,
        });
    }

    handleModalClose(closeModal) {
        closeModal();
    }

    handleSave(save, closeModal) {
        this.setState({
            loading: true,
        });

        save();
        closeModal();
    }

    async storeCallback() {
        await this.setUsers();
    }

    createCustomModalHeader = (closeModal, save) => {
        return (
            <div className='modal-header'>
                <h3>Create user</h3>
            </div>
        );
    };

    createCustomInsertButton = (onClick) => {
        return (
            <button className='btn btn-primary' onClick={ onClick }>New user</button>
        );
    };

    createCustomModalFooter = (closeModal, save) => {
        return (
            <InsertModalFooter
                saveBtnText='Create'
                closeBtnText='Close'
                closeBtnContextual='btn-danger'
                saveBtnContextual='btn-success'
                onModalClose={ () => this.handleModalClose(closeModal) }
                onSave={ () => this.handleSave(save, closeModal) }
            />
        );
    };

    createCustomModalBody = (columns, validateState, ignoreEditable) => {
        return (
            <CreateNewUserBody
                validateState={ validateState }
                columns={ columns }
                ignoreEditable={ ignoreEditable }
                apiUsers={ this.apiUsers }
                storeCallback={ this.storeCallback.bind(this) }
            />
        );
    };

    handleDeleteButtonClick = (onClick, a) => {
        onClick();
    };

    createCustomDeleteButton = (onClick) => {
        return (
            <DeleteButton
                btnText='Delete'
                btnContextual='btn-danger'
                btnGlyphicon='glyphicon-edit'
                onClick={ e => this.handleDeleteButtonClick(onClick) }
            />
        );
    };

    confirmDelete = (next, dropRowKeys) => {
        let table = this.refs.userTable.state;
        let apiUsers = this.apiUsers;

        dropRowKeys.map(function (item, key) {
            for (let user of table.data) {
                if (user.id === item) {
                    apiUsers.destroyUser(user.id);
                }
            }
        });

        next();
    };

    renderLoading(): any
    {
        return <div className='loading'>
            <Loader
                type="Puff"
                color="#00BFFF"
                height="100"
                width="100"
            />
            <p>Hostinger front loading...</p>
        </div>;
    }

    render(): any
    {
        const selectRow = {
            mode: 'checkbox'
        };

        const options = {
            insertModalHeader: this.createCustomModalHeader,
            insertBtn: this.createCustomInsertButton,
            insertModalFooter: this.createCustomModalFooter,
            insertModalBody: this.createCustomModalBody,
            deleteBtn: this.createCustomDeleteButton,
            handleConfirmDeleteRow: this.confirmDelete,
        };

        return this.state.loading ? this.renderLoading() : (
          <div className="App">
            <br/>

            <header className="App-header">
                <h4>USER MANAGEMENT</h4>
            </header>

            <br/>

            <BootstrapTable
                data={this.state.users}
                selectRow={ selectRow }
                deleteRow={ true }
                options={ options }
                striped 
                hover 
                condensed
                insertRow
                tableBodyClass='table-style' 
                tableHeaderClass='table-style'
                containerClass='table-container'
                ref='userTable'
            >
                <TableHeaderColumn isKey dataField='id' width='50'>
                    ID
                </TableHeaderColumn>
                <TableHeaderColumn dataField='email'>
                    EMAIL
                </TableHeaderColumn>
            </BootstrapTable>
          </div>
        );
    }
}

export default App;
