import React from 'react';

class CreateNewUserBody extends React.Component {
    async getFieldValue() {
        let apiUsers = this.props.apiUsers;
        let data = {
            email: this.refs.email.value,
            password: this.refs.password.value,
        };

        const status = await apiUsers.storeUser(data);
        if (status) {
            alert('User successfully stored');
        } else {
            alert('User failed to store');
        }

        this.props.storeCallback();

        return data;
    }

    render() {
        return (
            <div className='modal-body'>
                <div>
                    <div className='form-group' key='email'>
                        <label>Email</label>
                        <input ref='email' type='text' defaultValue={ '' } className='form-control' />
                    </div>

                    <div className='form-group' key='password'>
                        <label>Password</label>
                        <input ref='password' type='password' defaultValue={ '' } className='form-control' />
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateNewUserBody;