// @flow

const OK = 'OK';
const FAIL = 'FAIL';

type GetUserResponseDataType = {|
    status: OK | FAIL,
    data: Object<{
        change_password: Number,
        created_as_draft: Boolean,
        created_at: String,
        email: String,
        email_verified_at: String,
        id: Number,
        is_admin: Boolean,
        is_draft: Boolean,
        name: String,
        updated_at: String,
    }>,
|};

type StoreUSerType = {|
    email: String,
    password: String
|};

type GetAllUsersResponseDataType = Array<GetUserResponseDataType>;

class ApiUsersService
{
    LINK_GET_USERS = 'api/oauth/users';

    oAuth = null;

    constructor(oAuth)
    {
        this.oAuth = oAuth;
    }

    async getAllUsers(): Promise<GetAllUsersResponseDataType|Boolean>
    {
        try {
            await this.oAuth.setValidToken();

            const isToken = await this.oAuth.checkToken();
            if (!isToken) {
                console.log('Fail to get API session key. Please contact with administrators!');

                return false;
            }

            let response = await fetch(
                process.env.REACT_APP_BACKEND_URL +
                this.LINK_GET_USERS,
                {
                    method: 'GET',
                    headers: await this.oAuth.getDefaultHeaders(),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.status === OK && responseJson.data) {
                return responseJson.data;
            } else {
                const error = new Error('Something went wrong on api server!');
                console.log(error);

                return false;
            }
        } catch (error) {
            console.log(error);

            return false;
        }
    }

    async getUser(userId): Promise<GetUserResponseDataType|Boolean>
    {
        try {
            await this.oAuth.setValidToken();

            const isToken = await this.oAuth.checkToken();
            if (!isToken) {
                console.log('Fail to get API session key. Please contact with administrators!');

                return false;
            }

            let response = await fetch(
                process.env.REACT_APP_BACKEND_URL +
                this.LINK_GET_USERS + '/' + userId,
                {
                    method: 'GET',
                    headers: await this.oAuth.getDefaultHeaders(),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.status === OK && responseJson.data) {
                return responseJson.data;
            } else {
                const error = new Error('Something went wrong on api server!');
                console.log(error);

                return false;
            }
        } catch (error) {
            console.log(error);

            return false;
        }
    }

    async destroyUser(userId): Promise<Boolean>
    {
        try {
            await this.oAuth.setValidToken();

            const isToken = await this.oAuth.checkToken();
            if (!isToken) {
                console.log('Fail to get API session key. Please contact with administrators!');

                return false;
            }

            let response = await fetch(
                process.env.REACT_APP_BACKEND_URL +
                this.LINK_GET_USERS + '/' + userId,
                {
                    method: 'DELETE',
                    headers: await this.oAuth.getDefaultHeaders(),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.status === OK) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.log(error);

            return false;
        }
    }

    async storeUser(data: StoreUSerType): Promise<Boolean>
    {
        try {
            await this.oAuth.setValidToken();

            const isToken = await this.oAuth.checkToken();
            if (!isToken) {
                console.log('Fail to get API session key. Please contact with administrators!');

                return false;
            }

            let response = await fetch(
                process.env.REACT_APP_BACKEND_URL +
                this.LINK_GET_USERS,
                {
                    method: 'POST',
                    headers: await this.oAuth.getDefaultHeaders(),
                    body: JSON.stringify(data),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.status === OK) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.log(error);

            return false;
        }
    }

    async updateUser(userId, data): Promise<Boolean>
    {
        try {
            await this.oAuth.setValidToken();

            const isToken = await this.oAuth.checkToken();
            if (!isToken) {
                console.log('Fail to get API session key. Please contact with administrators!');

                return false;
            }

            let response = await fetch(
                process.env.REACT_APP_BACKEND_URL +
                this.LINK_GET_USERS + '/' + userId,
                {
                    method: 'DELETE',
                    headers: await this.oAuth.getDefaultHeaders(),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.status === OK) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.log(error);

            return false;
        }
    }
}

export default ApiUsersService
