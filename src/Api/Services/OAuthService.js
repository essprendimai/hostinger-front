// @flow

class OAuthService
{
    API_TOKEN = 'oauth/token';
    TOKEN_ID = 'OAUTH_TOKENS@ALL';

    constructor(backendUrl, clientId, clientSecret, clientEmail, clientPassword, tokenId)
    {
        this.BACKEND_URL = backendUrl;
        this.BACKEND_OAUTH_USER_CLIENT_ID = clientId;
        this.BACKEND_OAUTH_USER_CLIENT_SECRET = clientSecret;
        this.BACKEND_OAUTH_USER_EMAIL = clientEmail;
        this.BACKEND_OAUTH_USER_PASSWORD = clientPassword;

        if (tokenId) {
            this.TOKEN_ID = tokenId;
        }

        this.setTokenData(null);
    }

    async getTokenData()
    {
        const data = localStorage.getItem(this.TOKEN_ID);

        return JSON.parse(data);
    }

    async setTokenData(tokens)
    {
        await localStorage.setItem(this.TOKEN_ID, tokens ? JSON.stringify(tokens) : null);
    }

    async getToken()
    {
        try {
            const response = await fetch(
                this.BACKEND_URL +
                this.API_TOKEN,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': 'no-cors',
                    },
                    body: JSON.stringify(Object.assign({}, {
                        client_id: this.BACKEND_OAUTH_USER_CLIENT_ID,
                        client_secret: this.BACKEND_OAUTH_USER_CLIENT_SECRET,
                        scope: '',
                    }, this.getDefaultCredentials())),
                }
            );
            const responseJson = await response.json();
            if (response.status === 200 && responseJson.access_token) {
                return responseJson;
            }

            return null;
        } catch (error) {
            console.log(error);

            return null;
        }
    }

    /**
     ** Main set tokens method
     **/
    async setValidToken()
    {
        const today = new Date().getTime();

        try {
            let tokens = await this.getTokenData();
            if (tokens && tokens.token_expires <= today) {
                tokens = await this.tryRefresh(tokens);
            }

            if(!tokens) {
                await this.trySetNewToken()
            }
        } catch (error) {
            console.log(error);

            this.setTokenData(null);
        }
    }

    async tryRefresh(tokens)
    {
        let token = await this.tryRefreshToken(tokens);
        if (token) {
            this.setTokenData(this.formatToken(token));
        }

        return token;
    }

    async trySetNewToken()
    {
        let newToken = await this.getToken();
        if(newToken) {
            newToken = this.formatToken(newToken);

            await this.setTokenData(newToken);
        }

        return newToken;
    }

    async tryRefreshToken(tokens)
    {
        try {
            let response = await fetch(
                this.BACKEND_URL +
                this.API_TOKEN,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        client_id: this.BACKEND_OAUTH_USER_CLIENT_ID,
                        client_secret: this.BACKEND_OAUTH_USER_CLIENT_SECRET,
                        scope: '',
                        grant_type: 'refresh_token',
                        refresh_token: tokens.refresh_token,
                    }),
                }
            );

            let responseJson = await response.json();
            if (response.status === 200 && responseJson.access_token) {
                return responseJson;
            }

            if (responseJson.error || responseJson.hint === 'Token has expired') {
                this.setTokenData(null);
            }

            return null;
        } catch (error) {
            console.log(error);

            return null;
        }
    }

    formatToken(token)
    {
        let date = new Date();
        date.setSeconds(date.getSeconds() + token.expires_in);

        token.token_expires = date.getTime();

        return token;
    }

    getDefaultCredentials()
    {
        return {
            username: this.BACKEND_OAUTH_USER_EMAIL,
            password: this.BACKEND_OAUTH_USER_PASSWORD,
            grant_type: 'password',
        }
    }

    async getDefaultHeaders()
    {
        const tokens = await this.getTokenData();

        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + (tokens.access_token ? tokens.access_token : ''),
        };
    }

    async checkToken()
    {
        const tokens = await this.getTokenData();

        return !tokens ? false : true;
    }
}

export default OAuthService